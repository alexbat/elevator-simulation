# **Elevator Simulation**
---------------------

The Elevator Simulation program was created as a fun project in order to see how close I could get to simulating how elevators function in picking up and dropping off people at varying floors with differing loads.

# Running the Program

Use the SSH link in a command line to make a clone of the repository.


>$ git clone git@bitbucket.org:alexbat/elevator-simulation.git

Go to the Elevator directory and compile the .java files, with RunElevator being the main class to run.  

>$ cd src/asepulveda/elevator  
$ javac *.java  
$ java RunElevator <(OPTIONAL) nameOfTestFile.txt>

**Note:** If no input file is passed in, the program is designed to create its own test input file called *elevatorTestInput.txt* with pre-determined variables to create sudo random test queries.

If passing a self made input file, the first line of the file needs to be the number of floors followed by the number of elevators the program will use, both separated by a space.  Any subsequent line until EOF  will represent a new elevator query, with each query starting with the start floor first, ending floor second and the load(in terms of pounds) that will enter the elevator last.  

Both the start and end floor must be greater than or equal to zero and less than the number of floors from the first line.  The load value needs to be less than the set MAXSAFETYLOAD, which is 800lbs.  A query that has a zero value for all three components represents a "no new query cycle", simulating the times in which elevators are standing by for a user to press a button.  The file structure is as follows:

>numberOfFloors numberOfElevators  
startingFloor endingFloor load  
startingFloor endingFloor load  
startingFloor endingFloor load  
etc...

Example:
>10 3  
1 4 128  
5 2 150  
0 0 0  
9 3 190  
etc...


#  Limitations
Users of this program should be aware of the following limitations:

* The program was designed to emulate how elevators function using a finite set of floor inputs, which is determined by the test file passed in or created at run time; where as elevators in real life are constantly monitoring for any new queries 24/7.  

* The elevator is designed to travel +/-1 floor per cycle no matter the number of floors in the simulation.  In the real world, the number of floors an elevator travels per "cycle" increases up to a set speed as the number of floors increase.  A way to fix it would be to add a method to the Elevator class that increases the number of floors an elevator traverses for every factor of 10, and then modifying portions of the code to account for the speed of which the elevator is traversing up and down in order to not skip a pick up or drop off floor.  The fix would look something like this:  
***
```java
    
private int moveValueOffset(int numOfFloors){
    if (numOfFloors / 10 == 0 ? 1 : numOfFloors / 10);
}

static int UP = moveValueOffset(TOPFLOOR);
static int DOWN = -(moveValueOffset(TOPFLOOR));

public void updateMovement() {    	
    if ((destinationFloor - currentFloor) < NO_DIRECTION)
    	move = DOWN;
    else if ((destinationFloor - currentFloor) > NO_DIRECTION)
    	move = UP;
    else {
    	move =  NO_DIRECTION;	
    	direction = NO_DIRECTION;
    }
    System.out.println("Elevators move value is: " + move);
}
```
***

* The elevator has a fixed GROUNDFLOOR of 0, where as in real life there can be negative floor values, like the basement or underground parking floors.  To accommodate for this, GROUNDFLOOR would need to change to static int and the constructor would need to take another parameter to set the GROUNDFLOOR dynamically.  Both the Elevators elevatorQueue and FloorQueries arrays index-to-floor 1:1 relationship would need to be modified to accommodate for negative floor values, which can be done by creating a new variable to hold the difference between the new lowest floor and the first floor that is not underground, or the first street-level floor, in order to translate from array index values to the actual floor values and vice versa. 


* Lastly, the program does not account for other buttons an elevator can have , like the hold-door, close-door and ring-alarm button since they do not factor into the overall goal of this project.