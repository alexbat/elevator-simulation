package asepulveda.elevator;

/**
* The Elevator Class define the methods and values needed in order to simulate an
* elevator moving up and down a building with respect to the load and destination
* values it has at a given time.
* @author Alex Sepulveda.
*/
public class Elevator{
    private int currentFloor;
    private int destinationFloor;
    private int move;
    private int direction;
    private int currentLoad;
    private int elevatorID;
    private int[] elevatorQueue;
    
    static int TOPFLOOR;
    static final int MAXSAFETYLOAD = 800;
    static final int UP = 1;
    static final int DOWN = -1;
    static final int NO_DIRECTION = 0;
    static final int GROUNDFLOOR = 0;
    
    /** Constructor initializes variables to equal 0.
	 * @param numOfFloors int - Number of floors in simulation.
	 * @param id int - New Elevator's identification value.
	 */
    public Elevator(int numOfFloors, int id){
        currentFloor = GROUNDFLOOR;
		destinationFloor = GROUNDFLOOR;
        currentLoad =  0;
		elevatorID = id;
		move =  NO_DIRECTION;	
		direction = NO_DIRECTION;
        TOPFLOOR = numOfFloors;
        
        
        elevatorQueue = new int[numOfFloors];
        		
		for (int i = 0; i < numOfFloors; i++)
			elevatorQueue[i] = 0;
    }
    
    /** Method takes the elevators current floor and compares it to its destination floor
	 * and sets the elevators movement accordingly.
	 */
    public void updateMovement() {    	
    	if ((destinationFloor - currentFloor) < NO_DIRECTION)
    		move = DOWN;
    	else if ((destinationFloor - currentFloor) > NO_DIRECTION)
    		move = UP;
    	else {
    		move =  NO_DIRECTION;	
    		direction = NO_DIRECTION;
    	}
    	System.out.println("Elevators move value is: " + move);
    }

    /** Method returns whether the elevator can handle the new load or not.
	 * @param newLoad int - Load trying to be added to queue.
	 * @return Returns true or false if the elevator can handle the new load with respect
	 * 		to it's current load and the MAXSAFETYLOAD elevators are allowed to carry.
	 */
	public boolean canAddNewLoad(int newLoad){
		return (currentLoad + newLoad < MAXSAFETYLOAD ? true : false);
	}

	/** Method moves elevator to the next floor in its direction and checks to see if there 
	 * is a load to drop off at the new floor
	 */
	public void updateElevatorQueue(){
		
		//Change the current floor with respect to elevators current move value
		if(currentFloor + move >= GROUNDFLOOR || currentFloor + move < TOPFLOOR) {
        	System.out.print("Changing floor from: " + currentFloor);
        	currentFloor += move;
        	System.out.println(" to " + currentFloor);
        }  
		
		//Drop off load if the floor is not empty and update currentLoad
		if (elevatorQueue[currentFloor] != 0){
			System.out.print("Dropped off load: " + elevatorQueue[currentFloor]);
			currentLoad += elevatorQueue[currentFloor];
			elevatorQueue[currentFloor] = 0;
			System.out.println(" At floor " + currentFloor);
		}
		
		//Set up next iterations move value.  If elevator has reached destinationFloor,
		//move will reset to 0.
		updateMovement();
	}
	
	/** Method takes a new destination floor and load and adds it to the calling elevator's queue
	 * @param floor int - Floor to add to queue.
	 * @param load int - Load to add to queue.
	 */
	public void addToQueue(int floor, int load){
		
		System.out.println("Add " + floor + " floor and " + load + " load to elevetor.");
		elevatorQueue[floor] += -load; 
		
		//If new query is first in queue, set the destinationFloor to that floor 
		//and update the move.
		if (currentLoad == 0){
			changeFinalFloor(floor);
			updateMovement();
			updateDirection(move);
		}
		//Else If the new floor is the lowest or highest floor in relation 
		//to the elevator's move, update the new final floor accordingly.
		else if ((move < 0 && floor < destinationFloor) || (move > 0 && floor > destinationFloor))
			changeFinalFloor(floor);
		
		//Update elevators currentLoad with the new load it picked up
		currentLoad += load;
	}
	
	/** Method updates Elevators destinationFloor with given value
	 * @param floor int - Value to become new destinationFloor.
	 */
	public void changeFinalFloor(int floor) {destinationFloor = floor;}  
	
	/** Method updates Elevators direction with given value
	 * @param newDirection int - Value to become new direction.
	 */
    public void updateDirection(int newDirection) {direction = newDirection;}
    
    /** Method prints Elevators ID value. */
    public void printID() { System.out.println( "\nElevator ID: " + elevatorID); }
	
    
    /** @return Method returns Elevators currentFloor. */
    public int getCurrentFloor() {return currentFloor;}
    
    /** @return Method returns Elevators currentLoad. */
    public int getcurrentLoad() {return currentLoad;}
    
    /** @return Method returns Elevators move value. */
    public int getMove() {return move;}
    
    /** @return Method returns Elevators direction. */
    public int getDirection() {return direction;}
    
    /** @return Method returns Elevators destinationFloor. */
    public int getDestination() {return destinationFloor;}
}

