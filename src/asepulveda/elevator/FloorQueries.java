package asepulveda.elevator;

import java.util.LinkedList;

/**
* The Purpose of this Class is to be able to create an array of Linked
* Lists stored together under an object.  The size of the FloorQueries Object array 
* represents the number of floors in the simulation.  In each "building floor" or 
* index in the array is a Linked List that stores an array of integers.  The indexes
* for the FloorQueries represent the "starting floor" in which a call for an elevator is 
* being made.  The LinkedList at that index, or "floor", will hold individual int arrays 
* which store the "ending floor" and "load" that will enter the elevator.  Each floor is 
* coded as a LinkedList since in order to potentially store an undetermined amount 
* of queries at each floor with different ending floors and loads before the elevator arrives.
* @author Alex Sepulveda.
*/
class FloorQueries extends LinkedList<int[]> {
	
	//Extending a LinkedList requires that we create serial number
	//to identify this version.  The number generated is a random set
	//of numbers unique to the author and therefore can be changed.
	private static final long serialVersionUID = 241310L;
	
	public FloorQueries() {
		super();
	}
}