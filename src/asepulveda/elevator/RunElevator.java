package asepulveda.elevator;

import java.io.*;
import java.util.LinkedList;

/**
* The Purpose of this Class is to simulate how Elevators in the real world work.  A filename
* can be passed in as an argument to run the simulation on, else the program will create
* one using the CreateElevatorTestInput class.  The simulation will run until EOF is
* reached, there are no more pending queries and all of the elevators have delivered their
* loads and reset back to the GROUDFLOOR.
* @author Alex Sepulveda.
*/
public class RunElevator {
	static final int ARRAY_ENDFLOOR = 0;
	static final int ARRAY_LOAD = 1;
	
	public static void main(String[] args) {
		
		String parsedFileInput[], currentFileLine, fileName;
		FloorQueries[] upQueries, downQueries;
		Elevator[] elevatorList;
		int numOfFloors, numOfElevators, i, direction;
		boolean continueSteppingThroughElevators = true;
		
		//Check to see if a file name is passed in as an argument
		//else use the CreateElevatorTestInput class to create a test file
		if (args.length == 1) 
			fileName = args[0];
		else {
			CreateElevatorTestInput.createTestFile();
			fileName = "elevatorTestInput.txt";
		}
		
		//Try to open the file and begin the program
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))){
			
			//First line in file will always be the number of floors and elevators
			parsedFileInput = br.readLine().split(" ");
			numOfFloors = Integer.parseInt(parsedFileInput[0]);
			numOfElevators = Integer.parseInt(parsedFileInput[1]);
			
			//Set the size of the FloorQuery and Elevator Arrays
			upQueries = new FloorQueries[numOfFloors];
			downQueries = new FloorQueries[numOfFloors];
			elevatorList = new Elevator[numOfElevators];
			
			//Initialize the list of Elevator Objects
			for (i = 0; i < numOfElevators; i++)
				elevatorList[i] = new Elevator(numOfFloors, i);
			
			//Initialize the Up and Down Queries with a Linked List for each floor
			for (i = 0; i < numOfFloors; i++) {
				upQueries[i] = new FloorQueries();
				downQueries[i] = new FloorQueries();
			}			
			
			//While not EOF, read the next file line, load the correct elevator query
			//and move the elevators accordingly.  Each iteration of the loops acts as a 
			//cycle of the elevators shifting up or down accordingly. 
        	while ((currentFileLine = br.readLine()) != null) {
        		
        		//Split the read file line by spaces and store each value
        		//into a String array
        		parsedFileInput = currentFileLine.split(" ");
        		       		
        		//Check to see if the read line is a new query and what direction it is for
        		// 0 = no new query , -1 = new down query, 1 = new up query.  If direction is 0
        		//no new query will be added to either list.
    			direction = Integer.parseInt(parsedFileInput[1]) - Integer.parseInt(parsedFileInput[0]);		
    			if (direction > 0)
    				newElevatorEntry(upQueries, parsedFileInput);
    			else if (direction < 0)
    				newElevatorEntry(downQueries, parsedFileInput);    			
    			
    			//After adding a new queue to either of the two list, cycle through elevators
    			//with both UP and DOWN queries and update each elevator according to their 
    			//status and load.
    			stepThroughElevatorQueries(upQueries, downQueries, elevatorList, true);    			
        	}
		}
		//Any exceptions thrown will be caught under one generic catch statement since
		//most errors a stemming from the format of the input file
		catch (IOException | ArrayIndexOutOfBoundsException | NumberFormatException exc) {
			System.out.println("\nTest input file formated incorrectly:");
			System.out.println(exc);
			System.out.println("\nError Occured at: ");
			exc.printStackTrace();
			System.out.println("\nPlease correct and try again");
        	return;
		}
		
		//EOF reached, continue cycling through all of the floor and both lists
		//until there are no more pending queues to be picked up.  
		i = 0;
		while (continueSteppingThroughElevators) {
			//If a floor has been proven to be empty for both lists, no need to 
			//check it again for each elevator movement cycle.
			for ( ; i < numOfFloors; i++) {
				if (upQueries[i].size() > 0 || downQueries[i].size() > 0) {
					stepThroughElevatorQueries(upQueries, downQueries, elevatorList, true);
					break;
				}
				//All floors have no pending queries, end the while loop
				else if (i == numOfFloors - 1)
					continueSteppingThroughElevators = false;
			}
		}
		
		//Check if any elevators either have a load to drop off or have not reset back to 
		//starting position, else continue cycling through all of the elevators 
		i = 0;
		continueSteppingThroughElevators = true;
		while (continueSteppingThroughElevators) {
			//If an elevator has been proven to be reset and empty, no need to 
			//check it again for each elevator movement cycle.
			for ( ; i < numOfElevators; i++) {
				if (elevatorList[i].getMove() != 0 || (elevatorList[i].getMove() == 0 && elevatorList[i].getCurrentFloor() != Elevator.GROUNDFLOOR)) {
					stepThroughElevatorQueries(upQueries, downQueries, elevatorList, false);
					break;
				}
				//All elevators have no pending queries and have reset, end the while loop
				else if (i == numOfElevators - 1)
					continueSteppingThroughElevators = false;
			}
		}
	}
	
	/**
	 * This method adds a new entry to the given floorList[] according to the values read in
	 * from the current Input file line.
	 * @param floorList FloorQueries[] - List that new query will be added to.
	 * @param fileValues String[] - Array of values read from the current Input file line.
	 * @throws ArrayIndexOutOfBoundsException If the start or ending floor place the
	 * 		array index out of bounds with respect to the GROUNDFLOOR and TOPFLOOR, 
	 * 		throw an exception that the calling method will handle
	 */
	private static void newElevatorEntry(FloorQueries[] floorList, String[] fileValues) 
		throws ArrayIndexOutOfBoundsException, NumberFormatException{
		
		int[] dataArray = new int[2];
		
		int startFloor = Integer.parseInt(fileValues[0]);
		
		//Since values will always be in the same order, can just extract
		//file values directly from the array
		dataArray[ARRAY_ENDFLOOR] = Integer.parseInt(fileValues[1]);
		dataArray[ARRAY_LOAD] = Integer.parseInt(fileValues[2]);	
		
		//Check to see if the start or end floor from the input file will cause an
		//Array Index out of Bounds, or if the load is greater than or equal to 
		//the MAXSAFETYLOAD, if so throw an exception to end the program
		if ((startFloor < Elevator.GROUNDFLOOR || startFloor >= Elevator.TOPFLOOR) ||
		   (dataArray[ARRAY_ENDFLOOR] < Elevator.GROUNDFLOOR || dataArray[ARRAY_ENDFLOOR] >= Elevator.TOPFLOOR)) {
				System.out.println("\n\nERROR, startFloor and endFloor are: " + startFloor + " , " + dataArray[ARRAY_ENDFLOOR]);	
				throw new ArrayIndexOutOfBoundsException();
		}
		else if (dataArray[ARRAY_LOAD] >= Elevator.MAXSAFETYLOAD) {
			System.out.println("\n\nERROR, load value is: " + dataArray[ARRAY_LOAD]);
			throw new NumberFormatException();
		}
		
		//The Linked List of queries for each floor will be treated as FIFO
		if (floorList[startFloor].size() == 0) 
			floorList[startFloor].addFirst(dataArray);
		else 
			floorList[startFloor].addLast(dataArray);
	}	
	
	/**
	 * This method cycles through all of the elevators and updates their movement or 
	 * assigns them a new query according to their status and load.
	 * @param up FloorQueries[] - List of entries that want to travel UP.
	 * @param down FloorQueries[] - List of entries that want to travel DOWN.
	 * @param elevators Elevators[] - List of elevators to cycle through.
	 * @param checkForNewEntries boolean - Whether or not there are still entries in either of the FloorQueries[] lists.
	 */
	private static void stepThroughElevatorQueries(FloorQueries[] up, FloorQueries[] down, Elevator[] elevators, boolean checkForNewEntries) {
		
		Elevator currentElevator, comparingElevator;
		
		//Iterate through the list of elevators and update them according to their status
		for (int i = 0; i < elevators.length; i++) {
			
			currentElevator = elevators[i];
			currentElevator.printID();
			
			//If the elevator has a direction to move, update the floor accordingly 
			//and check the new floor for new queries
			if (currentElevator.getMove() != 0) {
				
				//Shift the current elevator up or down a floor and drop off a load
				//if need be.
				currentElevator.updateElevatorQueue();				
				
				//If the direction is positive (or if it has reached destinationFloor)
				//check the UP FloorQueries for any pending queues on the current floor
				//if yes, pop and add as many as the elevator can hold
				if (currentElevator.getMove() > 0 ||  currentElevator.getMove() == 0)
					addCurrentFloor(up[currentElevator.getCurrentFloor()], currentElevator);				
					
				//If the direction is negative (or if it has reached destinationFloor)
				//check the DOWN FloorQueries for any pending queues on the current floor
				//if yes, pop and add as many as the elevator can hold
				if (currentElevator.getMove() < 0 || currentElevator.getMove() == 0)
					addCurrentFloor(down[currentElevator.getCurrentFloor()], currentElevator);				
			}
			//If move is 0, check the UP and DOWN lists accordingly for new entries.
			if (currentElevator.getMove() == 0 && checkForNewEntries) {
				int j;
				LinkedList<Integer> upFloorsToCheck = new LinkedList<Integer>();
				LinkedList<Integer> downFloorsToCheck = new LinkedList<Integer>();
				LinkedList<Integer> skipFloors = new LinkedList<Integer>();
				
				//Iterate through the list of elevators and check the floors they will not be 
				//traversing through with respect to their direction to see if there are
				//any pending queues the current elevator can handle
				for (j = 0; j < elevators.length; j++) {
					comparingElevator = elevators[j];					
					
					//If comparing elevator to itself, skip to next comparison
					if (currentElevator.equals(comparingElevator))						
						continue;
					
					//Else if comparingElevator direction is UP, add the floors it will
					//not traverse to Up-LinkedList 
					else if (comparingElevator.getMove() > 0) {
						if (comparingElevator.getcurrentLoad()== 0) 
							skipFloors.add(comparingElevator.getDestination() * comparingElevator.getDirection());
						else
							addToCheckFloorList(upFloorsToCheck, Elevator.GROUNDFLOOR, comparingElevator.getCurrentFloor());
					}
					//Else comparingElevator direction is DOWN (or comparingElevator is going to reset),
					//add the floors it will not traverse to Down-LinkedList
					else if (comparingElevator.getMove() < 0 || (comparingElevator.getMove() == 0 && comparingElevator.getCurrentFloor() != Elevator.GROUNDFLOOR)) {
						if (comparingElevator.getcurrentLoad()== 0) 
							skipFloors.add(comparingElevator.getDestination() * comparingElevator.getDirection());
						else
							addToCheckFloorList(downFloorsToCheck, comparingElevator.getCurrentFloor(), Elevator.TOPFLOOR - 1);
					}
				}
				//If movement changes, break from block
				moveChanged: {
					//If UP LinkedList is empty,check all the floors in the UP list
					
					if (upFloorsToCheck.size() == 0 ) {
						checkAllFloors(up, skipFloors, currentElevator, Elevator.UP);
					}
					//If DOWN LinkedList is empty and direction has not changed, check all the floors
					//in the DOWN list
					if (downFloorsToCheck.size() == 0 && currentElevator.getMove() == Elevator.NO_DIRECTION) {
						checkAllFloors(down, skipFloors, currentElevator, Elevator.DOWN);
					}
					//If direction has changed now, break out of block
					if (currentElevator.getMove() != 0)
						break moveChanged;
					
					//Check the floors added to both LinkedLists for new queries, starting
					//with the UP list.  If direction changes, break out of block early
					if (checkUntraveresdFloors(upFloorsToCheck, up, currentElevator))
						break moveChanged;
					checkUntraveresdFloors(downFloorsToCheck, down, currentElevator);				
				}				
			}
			//If at the end there are no new entries on either list for the elevator
			//to start moving to and it is currently not on the GROUNDFLOOR, begin
			//moving the elevator back down one cycle at a time.
			if (currentElevator.getMove() == 0 && currentElevator.getCurrentFloor() != Elevator.GROUNDFLOOR) {
				System.out.println("Elevator is Resetting");
				currentElevator.changeFinalFloor(currentElevator.getCurrentFloor() - 1);
				currentElevator.updateMovement();
				currentElevator.updateDirection(Elevator.DOWN);
			}
		}
		
	}
	
	/**
	 * This method updates a LinkedList of floors to check for new queries with respect to the
	 * other elevators position and movement direction.
	 * @param floors LinkedList<Integer> - Data structure to hold the list of floors.
	 * @param start int - Starting floor to check.
	 * @param end int - Ending floor to check.
	 */
	private static void addToCheckFloorList(LinkedList<Integer> floors, int start, int end) {
		
		for (int i = start; i <= end; i++) {
			//If floor already in list, do not duplicate
			if(floors.contains(i))
				continue;
			floors.add(i);	
		}
	}
	
	/**
	 * This method removes elements from the LinkedList of floors to check for new queries
	 * in the passed in FloorQueries list.  If a popped floor is not empty, add it to current
	 * elevator's list accordingly.
	 * @param floors LinkedList<Integer> - Remove elements from the list to check for queries.
	 * @param queueList FloorQueries[] - Check for queries with given list.
	 * @param elevator Elevator - New floors to be added to specified elevator.
	 * @return Returns true if the elevator was given a new queue or movement direction.
	 */
	private static boolean checkUntraveresdFloors(LinkedList<Integer> floors, FloorQueries[] queueList, Elevator elevator) {
		int poppedFloor;
		
		//If floors is not empty, begin removing elements from the list
		//and checking for new queries at the given FloorQueries index location.
		while (floors.size() > 0) {
			poppedFloor = floors.pop();
			if (!queueList[poppedFloor].isEmpty()) {
				//If the elevator is not on the same floor, only change the elevators movement
				//in order to go pick up the new queue
				if (elevator.getCurrentFloor() != poppedFloor) {
					elevator.changeFinalFloor(poppedFloor);
					elevator.updateMovement();
				}
				else
					addCurrentFloor(queueList[poppedFloor], elevator);
				return true;
			}
		}		
		return false;
	}
	
	/**
	 * This method checks all of the floors with respect to the given FloorQueries[] list.
	 * If a value in the LinkedList has the same floor and direction as the current floor
	 * being checked, skip that floor since another elevator is enroute to that floor already.
	 * @param queueList FloorQueries[] - Check for queries with given list.
	 * @param skipFloors LinkedList<Integer> - skip any of the floors from this list.
	 * @param elevator Elevator - New floors to be added to specified elevator.
	 * @param direction int - The direction for which the list of elements are being inspected for.
	 */
	private static void checkAllFloors(FloorQueries[] queueList, LinkedList<Integer> skipFloors, Elevator elevator, int direction) {
		
		//Iterate through all of the floors with the given list
		for (int i = Elevator.GROUNDFLOOR; i < Elevator.TOPFLOOR; i++) {
			
			//If direction is positive, check for UP elements in the skipFloors list
			if (direction > Elevator.NO_DIRECTION) {
				if (skipFloors.contains(i))
					continue;
			}
			//Else direction is negative, check for DOWN elements in the skipFloors list
			else {
				if (skipFloors.contains(i * direction))
					continue;
			}
			//If the elevator is not on the same floor, update the elevators movement
			//in order to go pick up the new queue next cycle.
			if (!queueList[i].isEmpty()) {
				if (elevator.getCurrentFloor() != i) {
					elevator.changeFinalFloor(i);
					elevator.updateMovement();
					elevator.updateDirection(direction);
				}
				else
					addCurrentFloor(queueList[i], elevator);
				break;
			}
		}
	}
	
	/**
	 * If the given FloorQuerie floor is not empty, the method continues to remove elements from
	 * the list and add to the elevator's queue until either that floor is empty or adding
	 * a new entry will push the current elevator's load past he MAX SAFETY BOUND.
	 * @param currentFloor FloorQueries - Check for entries at given floor list..
	 * @param elevator Elevator - New floors to be added to specified elevator.
	 */
	private static void addCurrentFloor(FloorQueries currentFloor, Elevator elevator) {
		
		//While list is not empty, continue to try and add it to current elevator list
		while (!currentFloor.isEmpty()) {		
			
			//Check to see if the elevator can handle the new load.
			int[] elementFromList = currentFloor.peek();
			if (elevator.canAddNewLoad(elementFromList[ARRAY_LOAD])) {
				
				//Remove element from list and add it to elevators queue.
				elementFromList = currentFloor.pop();
				elevator.addToQueue(elementFromList[ARRAY_ENDFLOOR], elementFromList[ARRAY_LOAD]);
			}
			//Elevator cannot handle new queue, therefore exit loop.
			else
				break;
		}
	}
}

