package asepulveda.elevator;

import java.util.Random;
import java.io.*;

/**
* The Purpose of this Class is to create a test input file
* for the Elevator Program to run on.
* @author Alex Sepulveda.
*/
class CreateElevatorTestInput {
	
	/**
	 * This Method creates test a test input file dependent on the following variables:
	 *<p> 
	 *<ul> 
	 *	<li><b>int numOfSimulations</b> = The number of simulations to be created.</li>
	 *	<li><b>int numOfFloors</b> = How many floor there will be.</li>
	 *	<li><b>int numOfElevators</b> = The number of elevators available.</li>
	 *	<li><b>int probabilityOfNewQuery</b> = The probability of new query being generated.</li>
	 *</ul>
	 */
	static void createTestFile() {
		
		Random rand = new Random();
		PrintWriter pw = new PrintWriter(System.out, true);
		int minLoadRange = 80, maxLoadRange = 201; 
		int startFloor, endFloor, load;

		int numOfSimulations = 10, numOfFloors = 10, numOfElevators = 3;
		int probabilityOfNewQuery = (int)(numOfFloors * 0.4);
		
		
		try (FileWriter fw = new FileWriter("elevatorTestInput.txt")){
			
			//First line in the file is the number of floors and elevators
			fw.write(numOfFloors + " " + numOfElevators + "\n");
			
			for (int i = 0; i < numOfSimulations; i++) {
				//New Random Query
				if (rand.nextInt(numOfFloors) <= probabilityOfNewQuery) {
					//Set random start floor
					startFloor = rand.nextInt(numOfFloors);
					
					//Verify start and end floors are different
					endFloor = diffFloors(rand.nextInt(numOfFloors), startFloor, numOfFloors - 1);
					
					//Load num will be a value between the min and max load range
					load = rand.nextInt((maxLoadRange - minLoadRange)) + minLoadRange;
				}
				//No new elevator query request
				else {
					startFloor = endFloor = load = 0;
				}
				
				//Write to file the values separated by commas
				fw.write(startFloor + " " + endFloor + " " + load + "\n");
			}
		}
		//Catch all exceptions from opening file to writing to file and print it out
		catch (IOException exc) {
			pw.println(exc);
		}
	}
	
	/**
	 * This Method ensures the end floor is different than the starting floor
	 * @param floor int - Ending floor.
	 * @param startFloor int -Starting floor.
	 * @param topFloor int - Highest floor in the building.
	 * @return Returns an int for end floor after comparing it to startFloor and topFloor
	 * and changing the value accordingly.
	 */
	private static int diffFloors (int floor, int startFloor, int topFloor) {
		if (floor == startFloor) {	

			if (floor == topFloor)
				return floor - 1;
			else
				return floor + 1;						
		}
		return floor;
	}
	
}
